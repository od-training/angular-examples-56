import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Video } from './types';

function embiggenAuthorNames(videos: Video[]) {
  return videos.map(
    vid => ({ ...vid, author: vid.author.toUpperCase() })
  );
}

const apiUrl = 'http://api.angularbootcamp.com/videos';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  currentVideo = new BehaviorSubject<Video>(undefined);

  constructor(private http: HttpClient) { }

  setCurrentVideo(v: Video) {
    this.currentVideo.next(v);
  }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(apiUrl)
      .pipe(
        map(embiggenAuthorNames),

// If you want to set the current video from the very start...
//
//        tap(videos => {
//          if (videos[0]) {
//            this.setCurrentVideo(videos[0]);
//          }
//        })
      )
    ;
  }
}

import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { VideoDataService } from '../../video-data.service';
import { Video } from '../../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent {

  currentVideo: Observable<Video> = this.svc.currentVideo;

  constructor(private svc: VideoDataService) {}
}

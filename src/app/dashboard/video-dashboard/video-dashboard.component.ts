import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { VideoDataService } from '../../video-data.service';
import { Video } from '../../types';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {

  videos$: Observable<Video[]>;
 
  constructor(private svc: VideoDataService) {
    this.videos$ = svc.loadVideos();
  }

  setSelectedVideo(v: Video) {
    this.svc.setCurrentVideo(v);
  }
}

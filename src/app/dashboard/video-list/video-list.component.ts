import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Observable } from 'rxjs';

import { VideoDataService } from '../../video-data.service';
import { Video } from '../../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videos: Video[];
  @Output() videoSelected = new EventEmitter<Video>();

  currentVideo: Observable<Video>;

  constructor(svc: VideoDataService) {
    this.currentVideo = svc.currentVideo;
  }

  setCurrentVideo(v: Video) {
    this.videoSelected.emit(v);
  }
}
